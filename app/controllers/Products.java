package controllers;

import models.Product;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

import views.html.*;
import views.html.products.details;

/**
 * Created by ag on 6/25/2015.
 */
public class Products extends Controller{


    private static final Form<Product> productForm = Form
            .form(Product.class);

    public  Result list() {
        List<Product> products = Product.findAll();
        return ok(list.render(products));
    }




    public  Result newProduct() {
        return ok(details.render(productForm));
    }
    public  Result details(String ean) {
        return TODO;
    }
    public  Result save() {
        //return TODO;
        Form<Product> boundForm = productForm.bindFromRequest();
        Product product = boundForm.get();
        product.save();
        return ok(String.format("Saved product %s", product));
    }
}
